package td1;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Iterator;

public class ListeChainee2<E> implements Iterable<E>{
	E element;
	ListeChainee2<E> next;

	public ListeChainee2() {
		this.element=null;
		this.next=null;
	}
	public ListeChainee2(E element) {
		this.element=element;
		this.next=new ListeChainee2();
	}
	public ListeChainee2(E element,ListeChainee2<E> next) {
		this.element=element;
		this.next=next;
	}
	public ListeChainee2(Collection<? extends E> c) {
		ListeChainee2<E> lc =this;
		for(E e :c) {
			lc.element = e;
			lc.next = new ListeChainee2<>();
			lc = lc.next;
		}
	}
	public boolean estVide() {
		return this.next == null;
	}

	public ListeChainee2<E> withFirst(E value) {
		return new ListeChainee2<>(value,next);
	}

	public void addFirst(E value) {
		this.next = new ListeChainee2<E>(this.element,this.next);
		this.element=value;
	}

	public E removeFirst(){
		if (estVide()) throw new NoSuchElementException();
		E tmp = this.element;
		this.element=this.next.element;
		this.next=this.next.next;
		return tmp;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder('[');
		ListeChainee2<E> lc = this;
		while (!lc.estVide()) {
			sb.append(lc.element);
			sb.append(" , ");
			lc = lc.next;
		}
		sb.append(']');
		return sb.toString();
		//virgule en plus a gérer
	}
	public static void main(String[] args) {
		ListeChainee2<String> prenoms = new ListeChainee2<String>(Arrays.asList(new String[]{"max","sel","jad"}));
		ListeChainee2<Integer> ints = new ListeChainee2<>();
		System.out.println(ints.estVide());
		System.out.println(prenoms.estVide());
		ListeChainee2<Integer> ints2 = ints.withFirst(42);
		System.out.println(ints2.estVide());
		System.out.println(prenoms.toString());
	}
	
	@Override
	public Iterator<E> iterator() {
		return new ListeChaineeIterator(this);
	}
	
	public boolean contains(Object O) {
		if(this.element.equals(O)) 
			return true;
		return !this.estVide() && this.next.contains(O);
	}
	
	public class ListeChaineeIterator<E> implements Iterator<E>{
		
		private ListeChainee2 it;
		
		public ListeChaineeIterator(ListeChainee2<E> it) {
			this.it = it;
		}
		
		@Override
		public boolean hasNext() {
			return !this.it.estVide();
		}

		@Override
		public E next() {
			
			if (this.it.estVide()) throw new NoSuchElementException();
//			E tmp = this.it.element;
			this.it = this.it.next;
			return null; //tmp;
		}

	}
}
