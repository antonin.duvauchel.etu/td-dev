package td2;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

public class ArrayQueue<T> implements Queue<T>{
	private T[] container;
	private int head;
	private int tail;
	
	@SuppressWarnings("unchecked")
	public ArrayQueue(int size) {
		this.container= (T[]) new Object[size];
		this.head =0;
		this.tail =0;
	}
	@Override
	public int size() {
		return this.tail - this.head;
	}

	@Override
	public boolean isEmpty() {
		return this.head == this.tail;
	}
	
	public Boolean isFull() {
		return this.tail == this.head-1 || this.head == 0 && tail == container.length-1;
	}
	
	@Override
	public boolean add(T t) {
		if(isFull()) return false; // ou exception si on retourne rien
		this.container[this.tail++] = t;
		if (tail == container.length)
			tail=0; // gestion de la circularité
		return true ;
	}
	
	@Override
	public T remove() {
		if(isEmpty()) throw new NoSuchElementException();
		T tmp = this.container[this.head++];
		if (this.head == container.length)
			head=0;
		return tmp;
	}
	
	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean offer(T e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T poll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T element() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T peek() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
}
