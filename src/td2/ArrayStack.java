package td2;

import java.util.EmptyStackException;

public class ArrayStack<T>{

	private T[] container;
	private int size; // nb d'éléments dans le tableau
	
	// Q1 : sommet = 17, valeur n-1
	
	@SuppressWarnings("unchecked")
	public ArrayStack(int taille) {
		this.container= (T[]) new Object[taille];
		this.size=0;
	}
	Boolean isEmpty() {
		return this.size==0;
	}
	Boolean isFull() {
		return this.size == this.container.length;
	}
	void push(T t) {
		if (isFull()) throw new IllegalStateException();
		this.container[size++]=t;
	}
	T pop() {
		if (isEmpty()) throw new EmptyStackException();
		return this.container[--size];
	}
	T peek() {
		if (isEmpty()) throw new EmptyStackException();
		return this.container[size-1];
	}
}
