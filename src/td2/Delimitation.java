package td2;

public class Delimitation {

	public static boolean verifDelim(String s) {
		ArrayStack<Character> stack = new ArrayStack<>(s.length());
		for (char c : s.toCharArray()) {
			if (c == '(' || c == '[' || c== '{')
				stack.push(c);
			else if (c == ')' || c == ']' || c== '}' && close(c,stack.peek())){
				stack.pop();
			}
		}
		return stack.isEmpty();
	}

	private static boolean close(char open, Character close) {
		
		return open == '(' && close==')' || open == '[' && close==']' || open == '{' && close=='}';
	}

	public static void main(String[] args) {
		System.out.println(verifDelim("())(()"));
		System.out.println(verifDelim("((()))"));
	}
}
