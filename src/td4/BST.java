package td4;

public class BST<T extends Comparable<T>>{

	private T key;
	BST<T> left,right;

	public BST(){
		this(null,null,null);
	}
	public BST(T key){
		this(key,new BST<>(),new BST<>());
	}
	private BST(T key, BST<T> left, BST<T> right) {
		this.key=key;
		this.left = left;
		this.right = right;
	}
	private boolean isEmpty() {
		return this.key==null 
				&&this.left==null
				&&this.right==null;
	}
	protected BST<T> bstMin(){
		if (this.left.isEmpty()) 
			return this;
		return this.left.bstMin();
	}
	protected T min() {
		return bstMin().key;
	}
	public boolean add(T key) {
		if(this.isEmpty()) {
			this.key=key;
			this.right =new BST<>();
			this.right= new BST<>();
			return true;
		}
		if(this.key.compareTo(key)<0)
			return this.left.add(key);
		if(this.key.compareTo(key)>0)
			return this.right.add(key);
		return false;
	}
	public T deleteMin() {
		BST<T> bstMin = bstMin();
		T tmp = bstMin.key;
		this.key = bstMin.right.key;
		if (!bstMin.right.isEmpty())
			this.right = bstMin.right.right;
		return tmp;
	}
}
